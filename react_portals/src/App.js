import React from 'react';
import './App.css';
import Item from './Item'
import ps4Pro from './img/ps4_pro.jpg'
import ps4 from './img/ps4.jpg'
import ps4Play from './img/ps4_play_edition.jpg'


class App extends React.Component {
  items = [
    {
      image : ps4Play,
      price : 8599,
      desc : 'PlayStation 4 1TB Days of Play Limited Edition (CUH-2208B)',
      id : 1
    },
    {
      image : ps4,
      price : 9499,
      desc : 'PlayStation 4 1TB Black (CUH-2208B) Bundle + Horizon Zero Dawn. Complete Edition + Detroit + The Last of Us + PSPlus 3 месяца (PS4)',
      id : 2
    },
    {
      image : ps4Pro,
      price : 11999,
      desc : 'PlayStation 4 Pro 1TB Black (CUH-7208B) Bundle + God Of War + Horizon Zero Dawn. Complete Edition (PS4)',
      id : 3
    },
  ]

  render() {
    return(
        <>
          <h1>PS Store</h1>
          <div className='appWrapper cont'>
            {this.items.map((item) => (<Item key = {item.id} item = {item} />))}
          </div>
          <p className='cont'>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, inventore, laborum.
            Ad adipisci architecto assumenda commodi consectetur cumque deleniti doloribus, facere
            ipsum iste itaque iure nam nobis porro quam qui ratione rerum tempore vero vitae.
            Autem cum deserunt incidunt ipsa laboriosam neque quaerat quisquam sapiente similique.
            Accusamus aliquid animi at cupiditate deserunt dolor doloribus ex fuga id itaque magnam,
            maxime, minus natus ut vero, voluptate voluptatibus! Ipsa iste itaque odio perferendis suscipit
            totam veniam voluptatem. Amet eaque ipsa nihil, numquam veritatis vero! A at aut, commodi dolor
            dolorem dolores doloribus ducimus eligendi eum expedita explicabo fuga labore laborum necessitatibus
            nemo nostrum numquam quae qui quidem quos repudiandae veritatis vero voluptatem. A asperiores cum,
            ea hic modi nam necessitatibus quasi quibusdam? Ab accusantium alias aliquam architecto cumque dolor
            esse eveniet illo illum in magnam magni maxime minima modi mollitia optio porro quae, quibusdam
            quidem ratione rem repellat reprehenderit sapiente tempora vitae voluptas voluptatum. Ab accusamus
            aliquid at aut commodi debitis deserunt ea et excepturi, explicabo fuga fugit, illum ipsa magnam maxime
            molestias nobis non nostrum odit officia perferendis qui quibusdam repellat repudiandae sint?
            A deserunt est excepturi facere natus saepe. A consequatur eaque facere, nam natus sapiente voluptas!
            Ab, magnam vitae?
          </p>
        </>
    )
  }
}

export default App;
