import React from 'react';
import HeadingItem from './HeadingItem';
import DescriptionItem from './DescriptionItem';

const SectionItem = (props) => (
    <section className='item'>
        <HeadingItem
            headingText={props.headingText}
            tagType={props.headingTagType}
        />

        <DescriptionItem
            descriptionText={props.descriptionText}
            tagType={props.descriptionTagType}
        />
    </section>
)

export default SectionItem