import React from "react";
import Button from "./Button";
import ThemeContext from './ThemeContext'

class Sidebar extends React.Component {

    static contextType = ThemeContext

    constructor(props){
        super(props)
        this.input = React.createRef()
    }

    changeColorHandler = () => {

        const value = this.input.current.value
        this.input.current.value = ''

        if (value.match(/^#[0-9,A-F]{6}$/i)){
            this.context.changeThemeColor(value)
        } else {
            return
        }
    }

    render(){
        return (
            <div className='sectionSidebar'>
                <p className='userOnline'>User online: 1584</p>
                <Button text='Go to chat...' />
                <hr/>
                <p>Change color to:</p>
                <label>
                    <input ref={this.input} placeholder='Full HEX color...'/>
                </label>
                <br/>
                <br/>
                <Button text='Change' clickHandler={this.changeColorHandler}/>
            </div>
        )
    }
}

export default Sidebar