import React, { Component } from 'react';
import './css/App.css';
import Profile from './components/Profile';

class App extends Component{
  constructor(props) {
    super(props)
    this.state = {
      age : 25
    }
  }

  newRandomAge = () => {
    this.setState({
      age : Math.ceil(Math.random() * 100)
    })
  }

  render () {
    return(
        <div>
          <button onClick={this.newRandomAge}>Rerender</button>
          {this.state.age < 50 ? (
              <Profile
                  name='Nicky'
                  surname='Black'
                  age={this.state.age}
                  gender='male'
                  about='something about me...'
              />) : (null)
          }
        </div>
    )
  }
}

export default App;
