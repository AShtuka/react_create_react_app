import React from 'react';

const DescriptionItem = (props) => {
    const Desc = props.tagType
    return <Desc>{props.descriptionText}</Desc>
}
export default DescriptionItem