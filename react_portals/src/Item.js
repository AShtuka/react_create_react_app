import React from 'react'
import { findDOMNode } from 'react-dom'
import Description from './Description'

class Item extends React.Component {
    state ={
        descriptionVisible : false
    }

    toggleDescription = (visibility) => {
        this.setState({ descriptionVisible : visibility})
        const desc = document.getElementById('itemDescription')
        const { x, y, width, height } = findDOMNode(this).getBoundingClientRect()
        
        if (visibility){
            desc.style.left = `${x}px`
            desc.style.top = `${y + height}px`
            desc.style.width = `${width}px`
            desc.style.display = 'block'
        } else {
            desc.style.display = 'none'
        }
    }

    render() {
        const { image, price, desc } = this.props.item
        return (
            <div className='item'
                 onMouseEnter={this.toggleDescription.bind(this, true)}
                 onMouseLeave={this.toggleDescription.bind(this, false)}
            >
                <img className='image' src={image} alt="NOT FOUND"/>
                {this.state.descriptionVisible ?
                    (<Description price ={price} desc = {desc} />)
                    :
                    (null)
                }
            </div>
        )
    }
}

export default Item