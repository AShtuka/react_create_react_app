import React, { Component } from 'react'
import { connect } from 'react-redux'
import { User } from '../components/User'
import { Page } from '../components/Page'
import { Login } from '../components/Login'
import { getPhotos } from '../actions/PageActions'
import { handleLogin } from '../actions/UserActions'

class App extends Component {
    render() {
        const { user, page, getPhotosAction, loginAction } = this.props;
        return (
            <div className="app">
                {user.firstName === 'Unknown' ?
                    (<Login  handleLogin={loginAction}
                             isFetching={user.isFetching}
                    />)
                    :
                    (<>
                        <User
                            firstName={user.firstName}
                            lastName={user.lastName}
                            isFetching={user.isFetching}
                            logo={user.logo}
                            />
                        <Page
                            photos={page.photos}
                            year={page.year}
                            isFetching={page.isFetching}
                            getPhotos={getPhotosAction}
                        />
                    </>)
                }
            </div>
        )
    }
}

const mapStateToProps = store => {
    return {
        user: store.user,
        page: store.page
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getPhotosAction: year => dispatch(getPhotos(year)),
        loginAction: (firstName, lastName) => dispatch(handleLogin(firstName, lastName))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
