import React, { Component } from 'react';
import Card from './card';

const users = [
    {
        author : "Sasha",
        age : 25,
        specialty : "BusDriver",
        id : 1
    },
    {
        author : "Nicky",
        age : 35,
        specialty : "Doctor",
        id : 2
    },
    {
        author : "Glory",
        age : 19,
        specialty : "Student",
        id : 3
    }
]

class UserList extends Component {
    render() {
        return (
            <div className='wrapper'>
                {users.map((user) => <Card key = {user.id} {...user}/>)}
            </div>
        )
    }
}

export default UserList