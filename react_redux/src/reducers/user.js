import { LOGIN_REQUEST, LOGIN_SUCCESS } from '../actions/UserActions'

const initialState = {
    firstName: 'Unknown',
    lastName: 'Unknown',
    isFetching: false,
    logo: {}
};

export function userReducer(state = initialState, action) {
    switch (action.type) {

        case LOGIN_REQUEST:
            return { ...state,
                isFetching: true
            };

        case LOGIN_SUCCESS:
            return { ...state,
                    isFetching: false,
                    firstName: action.payload.firstName,
                    lastName: action.payload.lastName,
                    logo: action.payload.logo
            };

        default:
            return state
    }
}