import React from "react";

const orders = [
    {
        date : '02.08.17',
        amount : '100',
        buyerName : 'Sasha Shtuka',
        id : 1
    },
    {
        date : '10.12.18',
        amount : '589',
        buyerName : 'Luk Wocker',
        id : 2
    },
    {
        date : '01.07.15',
        amount : '1258',
        buyerName : 'John Black',
        id : 3
    }
]

const Main = () => (
    <div className='sectionMain'>
        <table>

            <thead>
            <tr>
                <th>Date</th>
                <th>Amount</th>
                <th>Buyer Name</th>
            </tr>
            </thead>

            <tbody>
            {orders.map((order) => (
                <tr key={order.id}>
                    <td>{order.date}</td>
                    <td>{order.amount}</td>
                    <td>{order.buyerName}</td>
                </tr>
            ))}
            </tbody>
        </table>
    </div>
)

export default Main