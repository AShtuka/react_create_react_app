import React from 'react';
import './App.css';
import Header from'./Header';
import Main from './Main';
import Sidebar from './Sidebar';
import ThemeContext from './ThemeContext'

class App extends React.Component {

  state = { themeColor: '#aa5577'}

  changeThemeColor = (newColor) => (this.setState({ themeColor : newColor}))

  render() {
    return (
        <ThemeContext.Provider value={{
                                      themeColor : this.state.themeColor,
                                      changeThemeColor : this.changeThemeColor
                                      }}>
          <div className='appWrapper'>
            <Header />
            <Main />
            <Sidebar />
          </div>
        </ThemeContext.Provider>
    )
  }
}

export default App;
