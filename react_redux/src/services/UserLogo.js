export default class UserLogo {
    static loadUserLogo() {
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        const url = "http://i.pravatar.cc/300";

        return fetch(proxyurl + url)
            .then(res => res.arrayBuffer())
            .then(res => {
                const arrayBufferView = new Uint8Array( res );
                const blob = new Blob( [ arrayBufferView ], { type: "image/jpeg" } );
                const urlCreator = window.URL || window.webkitURL;
                return urlCreator.createObjectURL( blob );
            })
    }
}
