import React from 'react'
import PropTypes from 'prop-types'

export class Login extends React.Component {

    state = {firstName : '',
             lastName : '',
             firstNameClassName: 'form-control',
             lastNameClassName: 'form-control'
    };

    onBtnClick = e => {
        e.preventDefault();
        this.props.handleLogin(this.state.firstName, this.state.lastName)
    };

    changeHandler = ({ target : {  name,  value }}) => {
        const fieldName = [name];
        const className = fieldName +'ClassName';

        if(value) {
            (this.setState({[fieldName] : value, [className] : 'form-control is-valid'}));
        } else {
            (this.setState({[fieldName] : value, [className] : 'form-control is-invalid'}));
        }
    };

    validate = () => {
        const { firstName, lastName } = this.state;
        if (firstName && lastName) {
            return true
        }
        return false
    };

    render() {
        const { isFetching } = this.props;
        return (<>
            {isFetching ? (<p>Загрузка...</p>)
                :
                (<div className="container">
                <p className="h5 text-center mb-4">Hello, dear friend! Fill out the form to continue</p>
                <div className="row">
                    <div className="col-sm" />
                    <div className="col-sm">
                        <form>
                            <div className="form-row">
                                <input type="text"
                                       name='firstName'
                                       onChange={this.changeHandler}
                                       className={this.state.firstNameClassName}
                                       placeholder="First name"
                                />
                            </div>
                            <div className="form-row mt-2">
                                <input type="text"
                                       name='lastName'
                                       onChange={this.changeHandler}
                                       className={this.state.lastNameClassName}
                                       placeholder="Last name"
                                />
                            </div>
                            <div className="form-row justify-content-center mt-2">
                                <button type="submit"
                                        onClick={this.onBtnClick}
                                        className="btn btn-primary"
                                        disabled={!this.validate()}
                                >
                                    Sign in
                                </button>
                            </div>
                        </form>
                    </div>
                    <div className="col-sm" />
                </div>
            </div>)}
            </>
        )
    }
}

Login.propTypes = {
     handleLogin: PropTypes.func.isRequired
};