import React from 'react';

class InputForms extends React.Component {

    constructor(props) {
        super(props)
        this.input = React.createRef()
    }
    state = {
                inputForm : '',
                selectForm : '',
                studying : false,
                grade : 0
            }

    changeHandler = ({ target : { type, name, checked, value }}) =>
        (this.setState({[name] : type === 'checkbox' ? checked : value}))

    submitHandler = (e) => {
        e.preventDefault()
        alert('A author was submitted ' + this.input.current.value + this.state)
    }

    render() {
        return (
            <form onSubmit={this.submitHandler}>
                <label>
                    Name :
                    <input type="text"
                           name='inputForm'
                           defaultValue='Your name'
                           onChange={this.changeHandler}
                           ref={this.input}
                    />
                </label>

                <br />

                <label>
                    Subject :
                    <select type="text" name='selectForm' value={this.state.selectForm} onChange={this.changeHandler}>
                        <option value="php">PHP</option>
                        <option value="java">Java</option>
                        <option value="js">JavaScript</option>
                        <option value="nodeJs">NodeJS</option>
                    </select>
                </label>

                <label>
                    Studying :
                    <input type="checkbox"
                           name='studying'
                           checked={this.state.studying}
                           onChange={this.changeHandler}
                    />
                </label>

                <label>
                    Grade :
                    <input type="number"
                           name='grade'
                           value={this.state.grade}
                           onChange={this.changeHandler}/>
                </label>

                <br />

                <button type='submit'>Submit</button>
            </form>
        )
    }
}

export default InputForms