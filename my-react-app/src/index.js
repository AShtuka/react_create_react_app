import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import SectionItem from './components/SectionItem';
import Clock from './components/Clock';
import ActionLink from './components/ActionLink';
import LoginControl from './components/greeting/LoginControl';
import UserList from './components/userList/UserList';
import InputForms from './components/inputForms/InputForms';

ReactDOM.render(
    <div>
        <div className='wrapper'>
            <SectionItem
                headingText='HTML'
                headingTagType='h1'
                descriptionText='Transform into the DOM'
                descriptionTagType='p'
            />

            <SectionItem
                headingText='CSS'
                headingTagType='h2'
                descriptionText='Style DOM-element'
                descriptionTagType='div'
            />

            <SectionItem
                headingText='JavaScript'
                headingTagType='h3'
                descriptionText='Adds interactivity to elements'
                descriptionTagType='div'
            />
        </div>
        <App />
        <Clock />
        <ActionLink />
        <LoginControl />
        <UserList />
        <InputForms />
    </div>,
    document.getElementById('root')
);
