import React, { Component } from 'react';

class Clock extends Component{
    constructor(props) {
        console.log(1, 'constructorInClock')
        super(props)
        this.state = {
            date : new Date()
        }
    }

    componentDidMount() {
        console.log(3, 'componentDidMountInClock')
        this.timerID = setInterval(() => this.tick(), 1000)
    }

    componentWillUnmount() {
        console.log(4, 'componentWillUnmountInClock')
        clearInterval(this.timerID)
    }

    tick() {
        this.setState({
            date : new Date()
        })
    }

    render () {
        console.log(2, 'renderInClock')
        return(
            <div className='clock'>
                <h1>Hello, now</h1>
                <h2>{this.state.date.toLocaleTimeString()}.</h2>
            </div>
        )
    }
}

export default Clock;