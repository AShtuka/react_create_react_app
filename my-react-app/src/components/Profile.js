import React from 'react';

const Profile = (props) => (
    <div className='profile'>
        <img src="http://i.pravatar.cc/300" alt="IMG NOT FOUND"/>
        <h1>{props.author} {props.surname}</h1>
        <p>Age: {props.age}</p>
        <p>Sex: {props.gender}</p>
        <p>About me: {props.about}</p>
    </div>
)

export default Profile