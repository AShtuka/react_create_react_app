import React, { Component } from 'react';
import LoginButton from "./LoginButton";
import LogoutButton from "./LogoutButton";
import Greeting from "./Greeting";

class LoginControl extends Component {
    constructor(props){
        super(props)
        this.state ={ isLoggedIn : false}
    }

    loginClickHandler = (e) => {
        this.setState({ isLoggedIn : true})
    }

    logoutClickHandler = (e) => {
        this.setState({ isLoggedIn : false})
    }

    render() {
        const isLoggedIn = this.state.isLoggedIn
        // let button
        // if (isLoggedIn) {
        //     button = <LogoutButton onClick = {this.logoutClickHandler} />
        // } else {
        //     button = <LoginButton onClick = {this.loginClickHandler} />
        // }
        return (
            <div>
                <Greeting isLoggedIn = {isLoggedIn} />
                {isLoggedIn && <img src="http://i.pravatar.cc/150" alt=""/>}
                {
                    isLoggedIn ?
                        (<LogoutButton onClick = {this.logoutClickHandler} />)
                        :
                        (<LoginButton onClick = {this.loginClickHandler} />)
                }
                {/*{button}*/}
            </div>
        )
    }
}

export default LoginControl;