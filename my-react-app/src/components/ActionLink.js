import React, { Component } from 'react';

class ActionLink extends Component {

    constructor(props) {
        super(props)
        // this.toggleModalHandler = this.toggleModalHandler.bind(this)
    }

    state = { modalVisible : false}

    toggleModalHandler = (e) => {
        this.setState(
            prevState => ({ modalVisible: !prevState.modalVisible})
        )
    }

    // showModal = (e) => {
    //     e.preventDefault();
    //     console.log('showModal');
    // }

    render() {
        // return (
        //     <div>
        //         <a href="http://makeweb.me" onClick={this.showModal}>
        //             Show modal, not go to link
        //         </a>
        //     </div>
        // )
        return (
            <div>
                {this.state.modalVisible ?
                    (
                        <div>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Alias delectus dolorem doloremque error harum nobis perferendis suscipit!
                            Accusantium corporis deleniti, enim facilis fugit iure, iusto labore laudantium
                            odit officiis porro quis repudiandae sed, sequi voluptatibus. Ab aliquam animi
                            at consequatur consequuntur culpa cum cupiditate dignissimos distinctio dolore
                            earum eveniet explicabo fuga fugiat id iure iusto labore libero maiores nobis
                            perferendis placeat praesentium quaerat quas, quis quod quos sed vero voluptate voluptates.
                            Adipisci alias amet asperiores commodi dicta eligendi eveniet fuga illo inventore iste
                            labore magnam quis quisquam, quod recusandae rerum suscipit, voluptate. Blanditiis consequuntur
                            itaque molestias officiis praesentium totam voluptate.
                        </div>
                    ):(null)
                }
                <button onClick={this.toggleModalHandler}>toggleButton</button>
            </div>
        )
    }
}

export default ActionLink