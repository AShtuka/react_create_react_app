import UserLogo from "../services/UserLogo";

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';

export function handleLogin(firstName, lastName) {
    return function(dispatch) {

        dispatch({
            type: LOGIN_REQUEST
        });

        UserLogo.loadUserLogo().then(res => {
            dispatch({
                type: LOGIN_SUCCESS,
                payload: {
                            firstName: firstName ,
                            lastName: lastName,
                            logo: res
                         }
            });
        });
    }
}

