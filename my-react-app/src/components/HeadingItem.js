import React from 'react';

const HeadingItem = (props) => {
    const Heading = props.tagType
    return <Heading>{props.headingText}</Heading>
}

export default HeadingItem