import React from 'react'
import { createPortal } from 'react-dom'

class Description extends React.Component {
    render() {
        const { price, desc } = this.props
        return createPortal(
            <>
                <p>Price: {price}</p>
                <p>Description: {desc}</p>
            </>,
            document.getElementById('itemDescription')
        )
    }
}

export default Description