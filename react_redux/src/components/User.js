import React from 'react'
import PropTypes from 'prop-types'

export class User extends React.Component {
    render() {
        const { firstName, lastName, isFetching, logo} = this.props;
        return (<>
                {isFetching ? (<p>Загрузка...</p>) :
                    (<div className="card" style={{width: 300 + 'px', margin: 0 + ' auto'}} >
                    <img src={logo} alt="IMG NOT FOUND"/>
                    <div className="card-body">
                        <h5 className="card-title h5 text-center">Привет, {firstName} {lastName}!</h5>
                        <p className="card-text text-center mb-2">How many picture do you wish download?</p>
                        <select className="custom-select mr-sm-2" id="inlineFormCustomSelect">
                            <option defaultValue='Choose'>Choose...</option>
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                        </select>
                    </div>
                </div>)}
            </>
        )
    }
}

User.propTypes = {
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired
};
