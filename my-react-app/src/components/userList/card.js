import React from 'react';

const Card = (props) => {
    const {name, age, specialty} = props
    return (
        <div className='item'>
            <img src="http://i.pravatar.cc/300" alt="IMG NOT FOUND"/>
            <h1>{name}</h1>
            <p>Age: {age}</p>
            <p>Specialty: {specialty}</p>
        </div>
    )
}

export default Card